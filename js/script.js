"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gREADY_STATE_REQUEST_DONE = 4;
var gSTATUS_REQUEST_DONE = 200;

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

$("#btn-check-voucher").on("click", onBtnCheckVoucherClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

// Hàm này để xử lý sự kiện nút check voucher click
function onBtnCheckVoucherClick(){
  console.log("clicked");  
  
  // B1: lấy giá trị nhập trên form, mã voucher (Thu thập dữ liệu)

  var vInputVoucher = $('#voucher');
  var vDivResultCheck = $('#div-result-check');

/*  console.log("Dư liệu được lấy từ");
  console.log("Input, id= " + vInputVoucher.attr('id') + '- placeholder: ' + vInputVoucher.attr('placeholder'));
  console.log("Tác động vào");
  console.log("Div, id= " + vDivResultCheck.attr('id') + '- innerHTML: ' + vDivResultCheck.html());
*/

  //khai báo đối tượng voucher
  var vVoucherObj = {
      maGiamGia: "",  
  }

  //đọc giá trị từ web
  readDataWeb(vVoucherObj);
   
  // B2: Validate data
  var vIsValidateData = validateData(vVoucherObj.maGiamGia);

  if(vIsValidateData) {
    // B3: Tạo request và gửi mã voucher về server
    var bXmlHttp = new XMLHttpRequest();
    sendVoucherToServer(vVoucherObj.maGiamGia, bXmlHttp);
    // B4: xu ly response khi server trả về
    bXmlHttp.onreadystatechange = function() {
      if(bXmlHttp.readyState === gREADY_STATE_REQUEST_DONE
        && bXmlHttp.status === gSTATUS_REQUEST_DONE) {
          processResponse(bXmlHttp);
        }
    }
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

//đọc dữ liệu từ web
function readDataWeb(paramdata){
  var vInputVoucher = $('#voucher').val();
  paramdata.maGiamGia = vInputVoucher;
}

// Hàm này dùng để validate data
function validateData(paramVoucher) {
  var vResultCheckElement = $('#div-result-check');
  if(paramVoucher === "") {
    vResultCheckElement.html("Mã giảm giá chưa nhập!");
    vResultCheckElement.prop('class', 'text-danger');
    return false;
  }
    vResultCheckElement.html("");
    vResultCheckElement.prop('class', 'text-dark');
    return true;
}

// Ham thuc hien viec call api va gui ma voucher ve server
function sendVoucherToServer(paramVoucher, paramXmlVoucherRequest) {
  paramXmlVoucherRequest.open("GET", "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucher, true);
  paramXmlVoucherRequest.send();
}

// Hàm này được dùng để xử lý khi server trả về response
function processResponse(paramXmlHttp) {
  
  var vJsonVoucherResponse = paramXmlHttp.responseText;
  
  var vVoucherResObj = JSON.parse(vJsonVoucherResponse); 
  console.log(vJsonVoucherResponse);
  
  var vDiscount = vVoucherResObj.phanTramGiamGia;
  var vResultCheckElement = $("#div-result-check");
      vResultCheckElement.html("Mã giảm giá " + vDiscount +"%"); 
} 
